import { Component, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-payroll-requests-report',
  templateUrl: './payroll-requests-report.component.html',
  styleUrls: ['./payroll-requests-report.component.css']
})
export class PayrollRequestsReportComponent implements OnInit {
  @BlockUI('dashboard') blockUIDashboard: NgBlockUI;
  @ViewChild('myTable', { static: false }) table: any;

  filtered = false;

  breadcrumb: any = {
    mainlabel: 'Payroll Result Reports',
    links: [
      {
        name: 'Home',
        isLink: true,
        link: '/dashboard'
      },
      {
        name: 'Reports',
        isLink: false
      },
      {
        name: 'Payroll Result Reports',
        isLink: false
      }
    ]
  };

  selected = [];

  timeout: any;
  count = 0;

  rows: any = [];

  stylerows: any = [
    {
      name: 'Marban',
      position: 'Otto',
      office: '@mdo',
      age: '34',
      salary: '16000',
      startdate: '16/05/2017'
    },
    {
      name: 'Jacob',
      position: 'Thornton',
      office: '@fat',
      age: '36',
      salary: '12000',
      startdate: '16/05/2017'
    },
    {
      name: 'Albart',
      position: 'the Bird',
      office: '@twitter',
      age: '38',
      salary: '12000',
      startdate: '16/05/2017'
    },
    {
      name: 'Marken',
      position: 'Otto',
      office: '@mdo',
      age: '32',
      salary: '12000',
      startdate: '26/05/2017'
    },
    {
      name: 'Jacob',
      position: 'Thornton',
      office: '@fat',
      age: '34',
      salary: '67000',
      startdate: '16/05/2017'
    },
    {
      name: 'Larry',
      position: 'the Bird',
      office: '@twitter',
      age: '39',
      salary: '22000',
      startdate: '16/05/2017'
    },
    {
      name: 'Margi',
      position: 'Otto',
      office: '@mdo',
      age: '31',
      salary: '42000',
      startdate: '16/05/2017'
    },
    {
      name: 'Jhon',
      position: 'Thornton',
      office: '@fat',
      age: '40',
      salary: '52000',
      startdate: '16/05/2017'
    },
    {
      name: 'Larry',
      position: 'the Bird',
      office: '@twitter',
      age: '48',
      salary: '20000',
      startdate: '16/05/2018'
    },
    {
      name: 'Mark',
      position: 'Otto',
      office: '@mdo',
      age: '36',
      salary: '12000',
      startdate: '16/05/2017'
    }
  ];

  tableDetails: any = {
    total: this.stylerows.length,
    uploaded: 0,
    manual: 0,
    deleted: 0
  };

  editing: any = {};

  constructor(private ngbModal: NgbModal) {
    this.stylerows.forEach(element => {
      element['id'] = this.count;
      this.count++;
    });
  }

  ngOnInit() {
    this.stylerows.forEach(element => {
      element['validated'] = true;
    });
  }

  addRow() {
    this.stylerows.push({});
    this.stylerows = [...this.stylerows];
  }
  reloadChangelog() {
    this.blockUIDashboard.start('Loading..');

    setTimeout(() => {
      this.blockUIDashboard.stop();
    }, 2500);
  }

  addARow() {
    this.stylerows.push({
      id: this.count,
      name: '-',
      position: '-',
      office: '-',
      age: '-',
      salary: '-',
      startdate: '-'
    });
    this.stylerows = [...this.stylerows];
    this.count++;
    this.tableDetails.manual++;
  }

  removeARow(rowId) {
    this.stylerows = this.arrayRemove(this.stylerows, rowId);
    this.tableDetails.manual--;
  }

  arrayRemove(array, rowId) {
    return array.filter(element => {
      return element.id !== rowId;
    });
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  uploadData() {
    this.rows = [...this.stylerows];
  }

  onValidate() {
    for (
      let i = this.stylerows.length - 1;
      i >= this.stylerows.length - 3;
      i--
    ) {
      this.stylerows[i].validated = false;
    }
    console.log(this.stylerows);
  }

  onMoreFilter() {
    this.filtered = !this.filtered;
  }
}
