﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { GlobalService } from '../_services/global.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  isTextFieldType: boolean;

  isInvalid = false;
  invalidText: string;

  customUser: any = {
    firstName: 'Admin',
    email: 'admin@gmail.com',
    password: 'admin',
    apiKey: 'AIzaSyAvHtu5rrqKSZQg75R2MYqlNgGdMKy2YR4'
  };

  constructor (
    private formBuilder: FormBuilder,
    private global: GlobalService,
    public authService: AuthService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['ALEX.SIBLEY@GKCO.COM', Validators.required],
      password: ['Gkco@123', Validators.required]
    });

    if (localStorage.getItem('currentUser')) {
      this.authService.doLogout();
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  tryLogin() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    const value = {
      username: this.f.email.value,
      password: this.f.password.value
    };
    this.authService.doLogin(value).subscribe (
      (response: any) => {
        if (response.token) {
          // Remove this once we get real user
          this.setUserInStorage(this.customUser);

          sessionStorage.setItem('token', response.token);
          localStorage.removeItem('currentLayoutStyle');
          this.global.goto('dashboard');
        } else {
          this.isInvalid = true;
          this.invalidText = 'Something went wrong.';
          setTimeout(() => (this.isInvalid = false), 2000);
        }
        this.submitted = false;
      },
      (error: HttpErrorResponse) => {
        this.isInvalid = true;
        if (error.status === 401) {
          this.invalidText = 'Invalid Email And Password.';
        } else {
          this.invalidText = 'Something went wrong.';
        }
        setTimeout(() => (this.isInvalid = false), 2000);
        this.submitted = false;
      }
    );
  }

  setUserInStorage(res) {
    if (res.user) {
      localStorage.setItem('currentUser', JSON.stringify(res.user));
    } else {
      localStorage.setItem('currentUser', JSON.stringify(res));
    }
  }

  togglePasswordFieldType() {
    this.isTextFieldType = !this.isTextFieldType;
  }
}
