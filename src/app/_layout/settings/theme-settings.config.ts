// Default theme settings configurations

export const ThemeSettingsConfig = {
  colorTheme: 'light', // light, semi-light, semi-dark, dark
  layout: {
    style: 'horizontal', // style: 'vertical', horizontal,
    pattern: 'static' // fixed, boxed, static
  },
  menuColor: 'navbar-light', // Vertical: [menu-dark, menu-light] , Horizontal: [navbar-dark, navbar-light]
  navigation: 'menu-collapsible', // menu-collapsible, menu-accordation
  menu: 'expand', // collapse, expand
  header: 'static', // fix, static
  footer: 'static', // fix, static
  customizer: 'off', // on,off
  brand: {
    brand_name: 'Grace Kennedy',
    logo: {
      type: 'internal', // internal, url
      value: 'assets/images/ico/favicon.ico' // recommended location for custom images
      // type:'url',
      // value:'http://evolvision.com/wp-content/uploads/2018/01/envelope4-green.png'
    },
  },
  defaultTitleSuffix: 'Grace Kennedy'
};
