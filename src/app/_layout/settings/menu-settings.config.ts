// Default menu settings configurations

export interface MenuItem {
  title: string;
  icon: string;
  page: string;
  isExternalLink?: boolean;
  issupportExternalLink?: boolean;
  badge: { type: string; value: string };
  submenu: {
    items: Partial<MenuItem>[];
  };
  section: string;
}

export interface MenuConfig {
  horizontal_menu: {
    items: Partial<MenuItem>[];
  };
  vertical_menu: {
    items: Partial<MenuItem>[];
  };
}

export const MenuSettingsConfig: MenuConfig = {
  horizontal_menu: {
    items: [
      {
        title: 'Home',
        page: '/dashboard'
      },
      {
        title: 'Administration',
        icon: 'feather ft-chevron-down',
        page: '/dashboard'
      },
      {
        title: 'Payroll',
        icon: 'feather ft-chevron-down',
        page: '/payroll/iccr',
        submenu: {
          items: [
            {
              title: 'Initiate Compensation Change Request',
              icon: 'la-money',
              page: '/payroll/iccr'
            },
            {
              title: 'Compensation Change Dashboard',
              icon: 'la-tasks',
              page: '/payroll/ccr'
            },
            {
              title: 'Payroll Result Dashboard',
              icon: 'la-money',
              page: '/payroll/prd'
            }
          ]
        }
      },
      {
        title: 'Reports',
        icon: 'feather ft-chevron-down',
        page: '/reports/ccrr',
        submenu: {
          items: [
            {
              title: 'Compensation Change Requests Report',
              icon: 'la-tasks',
              page: '/reports/ccrr'
            },
            {
              title: 'Payroll Requests Report',
              icon: 'la-money',
              page: '/reports/prr'
            }
          ]
        }
      }
    ]
  },
  vertical_menu: {
    items: [
      {
        title: 'Dashboard',
        icon: 'la-dashboard',
        page: '/dashboard'
      }
    ]
  }
};
