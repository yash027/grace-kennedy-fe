// Default theme settings configurations

export class NavigationConfig {
  public config: any = {};

  constructor() {
  this.config = {
      items: [
        {
          title: 'Dashboard',
          root: true,
          icon: 'la-home',
          page: '/dashboard',
        }
      ]
    };
  }
}
