import { Component, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/_services/http.service';

@Component({
  selector: 'app-initiate-compensation-change-request',
  templateUrl: './initiate-compensation-change-request.component.html',
  styleUrls: ['./initiate-compensation-change-request.component.css']
})
export class InitiateCompensationChangeRequestComponent implements OnInit {
  @BlockUI('dashboard') blockUIDashboard: NgBlockUI;
  @ViewChild('myTable', { static: false }) table: any;

  breadcrumb: any = {
    mainlabel: 'Initiate Compensation Change Request',
    links: [
      {
        name: 'Home',
        isLink: true,
        link: '/dashboard'
      },
      {
        name: 'Payroll',
        isLink: false
      },
      {
        name: 'Initiate Compensation Change Request',
        isLink: false
      }
    ]
  };

  selected = [];

  timeout: any;
  count = 0;

  rows: any = [];
  validatedRows: any = [];

  modalRows: any = [
    {
      id: 1,
      name: 'Alex',
      age: 24,
      group: 'A+'
    },
    {
      id: 2,
      name: 'Bob',
      age: 29,
      group: 'B+'
    },
    {
      id: 3,
      name: 'Carl',
      age: 19,
      group: 'C+'
    }
  ];

  isValidated: boolean;

  deletedCount: any = 0;
  manualCount: any = 0;

  constructor(private ngbModal: NgbModal, private http: HttpService) {}

  ngOnInit() {
    // this.http.GET('/webapp/api/init').subscribe( (response: any) => {
    //   console.log(response);
    // });
  }

  addRow() {
    const values = {
      id: this.count,
      employeeId: '',
      firstName: '',
      lastName: '',
      sbu: '',
      period: '',
      changeType: '',
      wageType: '',
      amount: '',
      goalAmount: '',
      assignNo: '',
      effectiveDate: '',
      endDate: '',
      isValidated: true
    };
    this.rows.push(values);
    this.rows = [...this.rows];
    this.count++;
    this.manualCount++;
  }
  reloadChangelog() {
    this.blockUIDashboard.start('Loading..');

    setTimeout(() => {
      this.blockUIDashboard.stop();
    }, 2500);
  }

  addARow() {}

  removeARow(rowId) {}

  arrayRemove(array, rowId) {
    return array.filter(element => {
      return element.id !== rowId;
    });
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  uploadData() {
    document.getElementById('fileUpload').click();
  }

  onValidate() {
    if (this.rows.length > 0 && !this.isValidated) {
      const min = 0;
      const max = this.rows.length - 1;
      const random = Math.floor(Math.random() * (max - min + 1)) + min;
      if (!(this.rows[random].employeeId === '')) {
        this.rows[random].isValidated = false;
        this.rows = [...this.rows];
        this.validatedRows = [...this.rows];
        this.isValidated = true;
      }
    }
  }

  onSort(isChecked) {
    if (this.validatedRows.length > 0) {
      if (isChecked) {
        this.rows = this.rows.filter(element => {
          return element.isValidated !== true;
        });
      } else {
        this.rows = [...this.validatedRows];
      }
    }
  }

  onRemove() {
    this.deletedCount += this.selected.length;
    this.manualCount -= this.selected.length;
    this.selected.forEach(element => {
      this.rows = this.arrayRemove(this.rows, element.id);
    });
    this.selected = [];
  }

  openModal(modal) {
    this.ngbModal.open(modal, {
      windowClass: 'animated fadeInDown',
      size: 'lg'
    });
  }

  onActivate(event: any) {
    // console.log(event);
  }

  onSelectFile(file) {
    // const data = file.target.files;
    // const formData = new FormData();
    // formData.append('file', data[0]);
    // this.http.POST('/webapp/api/excelValidation', formData).subscribe( response => {
    //   console.log(response);
    // }, error => {
    //   console.log(error);
    // });
  }
}
