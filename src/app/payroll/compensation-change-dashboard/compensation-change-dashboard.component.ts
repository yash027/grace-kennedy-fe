import { Component, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from 'src/app/_services/http.service';

@Component({
  selector: 'app-compensation-change-dashboard',
  templateUrl: './compensation-change-dashboard.component.html',
  styleUrls: ['./compensation-change-dashboard.component.css']
})
export class CompensationChangeDashboardComponent implements OnInit {
  @BlockUI('dashboard') blockUIDashboard: NgBlockUI;
  @ViewChild('myTable', { static: false }) table: any;

  selectedPeriod: any;

  breadcrumb: any = {
    mainlabel: 'Compensation Change Dashboard',
    links: [
      {
        name: 'Home',
        isLink: true,
        link: '/dashboard'
      },
      {
        name: 'Payroll',
        isLink: false
      },
      {
        name: 'Compensation Change Dashboard',
        isLink: false
      }
    ]
  };

  selected = [];

  timeout: any;
  count = 0;

  rows: any = [];

  stylerows: any = [
    {
      name: 'Marban',
      position: 'Otto',
      office: '@mdo',
      age: '34',
      salary: '16000',
      startdate: '16/05/2017'
    },
    {
      name: 'Jacob',
      position: 'Thornton',
      office: '@fat',
      age: '36',
      salary: '12000',
      startdate: '16/05/2017'
    },
    {
      name: 'Albart',
      position: 'the Bird',
      office: '@twitter',
      age: '38',
      salary: '12000',
      startdate: '16/05/2017'
    },
    {
      name: 'Marken',
      position: 'Otto',
      office: '@mdo',
      age: '32',
      salary: '12000',
      startdate: '26/05/2017'
    },
    {
      name: 'Jacob',
      position: 'Thornton',
      office: '@fat',
      age: '34',
      salary: '67000',
      startdate: '16/05/2017'
    },
    {
      name: 'Larry',
      position: 'the Bird',
      office: '@twitter',
      age: '39',
      salary: '22000',
      startdate: '16/05/2017'
    },
    {
      name: 'Margi',
      position: 'Otto',
      office: '@mdo',
      age: '31',
      salary: '42000',
      startdate: '16/05/2017'
    },
    {
      name: 'Jhon',
      position: 'Thornton',
      office: '@fat',
      age: '40',
      salary: '52000',
      startdate: '16/05/2017'
    },
    {
      name: 'Larry',
      position: 'the Bird',
      office: '@twitter',
      age: '48',
      salary: '20000',
      startdate: '16/05/2018'
    },
    {
      name: 'Mark',
      position: 'Otto',
      office: '@mdo',
      age: '36',
      salary: '12000',
      startdate: '16/05/2017'
    }
  ];

  validatedRows: any = [];

  tableDetails: any = {
    total: this.stylerows.length,
    uploaded: 0,
    manual: 0,
    deleted: 0
  };

  editing: any = {};

  constructor(private ngbModal: NgbModal, private http: HttpService) {
    this.stylerows.forEach(element => {
      element['id'] = this.count;
      this.count++;
    });
  }

  ngOnInit() {
    this.stylerows.forEach(element => {
      element['validated'] = true;
    });
    // this.http.GET('/webapp/api/init').subscribe( (response: any) => {
    //   console.log(response);
    // });
  }

  addRow() {
    this.stylerows.push({});
    this.stylerows = [...this.stylerows];
  }
  reloadChangelog() {
    this.blockUIDashboard.start('Loading..');

    setTimeout(() => {
      this.blockUIDashboard.stop();
    }, 2500);
  }

  addARow() {
    this.stylerows.push({
      id: this.count,
      name: '-',
      position: '-',
      office: '-',
      age: '-',
      salary: '-',
      startdate: '-'
    });
    this.stylerows = [...this.stylerows];
    this.count++;
    this.tableDetails.manual++;
  }

  removeARow(rowId) {
    this.stylerows = this.arrayRemove(this.stylerows, rowId);
    this.tableDetails.manual--;
  }

  arrayRemove(array, rowId) {
    return array.filter(element => {
      return element.id !== rowId;
    });
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  uploadData() {
    this.rows = [...this.stylerows];
  }

  onValidate() {
    for (let i = this.rows.length - 1; i >= this.rows.length - 3; i--) {
      this.rows[i].validated = false;
    }
    this.validatedRows = this.rows;
  }

  onSort(isChecked) {
    if (this.rows.length > 0) {
      if (isChecked) {
        this.rows = this.rows.filter(element => {
          return element.validated === false;
        });
      } else {
        this.rows = [...this.validatedRows];
      }
    }
  }
}
