import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PayrollServiceService {
  constructor(private http: HttpClient) {}

  getpayrolldata() {
    return this.http.get('http://localhost:4200/assets/json/data.json');
  }
}
