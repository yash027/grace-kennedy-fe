import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../_services/global.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private global: GlobalService) {}

  ngOnInit() {}

  changeLayoutHeader(url) {
    this.global.onChangeActiveNavLink.next(url);
  }
}
