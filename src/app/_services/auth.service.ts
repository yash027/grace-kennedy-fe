import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor(private http: HttpService, private httpClient: HttpClient) {}

  doLogin(data) {
    const url = '/authenticate';
    return this.http.POST(url, data);
  }

  doLogout() {
    localStorage.removeItem('currentUser');
    return true;
  }

}
