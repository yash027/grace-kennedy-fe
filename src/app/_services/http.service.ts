import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl: string = environment.url;

  constructor(private http: HttpClient) {}

  GET(url): Observable<any> {
    return this.http.get(this.baseUrl + url);
  }

  POST(url, body): Observable<any> {
    return this.http.post(this.baseUrl + url, body);
  }
}
