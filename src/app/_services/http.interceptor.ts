import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';
import { GlobalService } from './global.service';

@Injectable ()
export class HttpXsrfInterceptor implements HttpInterceptor {
  constructor(private global: GlobalService) {}

  intercept (
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let token: any;
    if (!req.url.includes('/authenticate')) {
      token = sessionStorage.getItem('token');
    }
    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        this.global.destroyUserSession();
        this.global.gotoLogin();
        return throwError(error);
      })
    );
  }
}
