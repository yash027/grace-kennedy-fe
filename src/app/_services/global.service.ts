import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable ({
  providedIn: 'root'
})
export class GlobalService {
  onChangeActiveNavLink: BehaviorSubject<any>;

  constructor(private router: Router) {
    this.onChangeActiveNavLink = new BehaviorSubject<any>(null);
  }

  goto (url: string) {
    this.router.navigate(['/' + url]);
  }

  gotoLogin() {
    this.goto('authenticate');
  }

  destroyUserSession() {
    localStorage.clear();
    sessionStorage.clear();
  }
}
