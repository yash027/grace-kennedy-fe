﻿import { PayrollRequestsReportComponent } from './reports/payroll-requests-report/payroll-requests-report.component';
import {
  CompensationChangeRequestReportComponent
 } from './reports/compensation-change-request-report/compensation-change-request-report.component';
import { Routes, RouterModule } from '@angular/router';
import { PublicLayoutComponent } from './_layout/public-layout/public-layout.component';
import { PrivateLayoutComponent } from './_layout/private-layout/private-layout.component';
import { AuthGuard } from './_guards/auth.guard';
import { LoginComponent } from './login';
import { DashboardComponent } from './dashboard/dashboard.component';
import {
  InitiateCompensationChangeRequestComponent
 } from './payroll/initiate-compensation-change-request/initiate-compensation-change-request.component';
import { PayrollResultDashboardComponent } from './payroll/payroll-result-dashboard/payroll-result-dashboard.component';
import {
  CompensationChangeDashboardComponent
 } from './payroll/compensation-change-dashboard/compensation-change-dashboard.component';

const appRoutes: Routes = [
  // Public layout
  {
    path: '',
    component: PublicLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '', redirectTo: 'login', pathMatch: 'full' }
    ]
  },
  // Private layout
  {
    path: '',
    component: PrivateLayoutComponent,
    children: [
      { path: 'logout', component: LoginComponent, canActivate: [AuthGuard] },
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            component: DashboardComponent
          }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: 'payroll',
        children: [
          {
            path: 'iccr',
            component: InitiateCompensationChangeRequestComponent
          },
          {
            path: 'ccr',
            component: CompensationChangeDashboardComponent
          },
          {
            path: 'prd',
            component: PayrollResultDashboardComponent
          },
          {
            path: '',
            redirectTo: 'iccr',
            pathMatch: 'full'
          }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: 'reports',
        children: [
          {
            path: 'ccrr',
            component: CompensationChangeRequestReportComponent
          },
          {
            path: 'prr',
            component: PayrollRequestsReportComponent
          },
          {
            path: '',
            redirectTo: 'ccrr',
            pathMatch: 'full'
          }
        ],
        canActivate: [AuthGuard]
      }
    ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(appRoutes, {
  scrollOffset: [0, 0],
  scrollPositionRestoration: 'top',
  useHash: true
});
